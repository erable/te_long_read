# Data origin and preprocessing

This document will explain where to find/how to generate data for the article 10.1101/2023.05.27.542554. 


## Reads

Reads are Nanopore RNASeq long-reads of *D. melanogaster* testis and ovary. A TeloPrime Full-Length cDNA Amplification kit was used to enrich full-length transcripts (see material and methods in 10.1101/2023.05.27.542554). Reads are available online at the BioProject **PRJNA956863**.

- **FC30.fastq** for testis
- **FC29.fastq** for ovaries

## Reference genome

A reference genome of *D. melanogaster* was used. The assembly of the strain **dmgoth101**, used in this analysis, is available at the BioProject **PRJEB50024**.

***Additional step** : This step was performed for 10.1101/2023.05.27.542554, but the following steps would still work if skipped. \
 Contig names were shortened to be more human-readable with the following command*

```sh
python3 ../src/rename_contig.py GCA_927717585.1.fasta > GCA_927717585.1.contig_named.fasta
```


## Gene annotation

**LiftOff** was used on FlyBase *D. melanogaster* annotation (dmel-all-r6.46.gtf.gz), with the option -flank 0.2.

***Additional step** : This step was performed for 10.1101/2023.05.27.542554, but the following steps would still work if skipped. \
The annotation file can be used to improve the alignment on minimap2. But beforehand, the .gtf must be converted to .bed. The paftools.js program from the minimap2 suite offers this conversion. However, to fit the paftools format standard, redundant information like exon and CDS features have to be deleted.*

```sh
grep -v CDS Dm_Goth_10-1.dmel6.23LiftOff.sorted.gff > Dm_Goth_10-1.dmel6.23LiftOff.sorted.noCDS.gff 
```

*Then we'll convert it in .bed with the paftools program from the minimap2 suite.*

```sh
paftools.js gff2bed Dm_Goth_10-1.dmel6.23LiftOff.sorted.noCDS.gff > Dm_Goth_10-1.dmel6.23LiftOff.sorted.noCDS.bed
```


## TE annotation (latest version)

We used **RepeatMasker** to annotate the dmgoth101 genome assembly with consensus from the MCTE library (https://www.nature.com/articles/s41467-022-29518-8).

```sh
./RepeatMasker -pa 15 -a -s -gff -cutoff 200 -no_is -lib MCTE_clem.fasta GCA_927717585.1.contig_named.fasta
```

We then used **RepeatCraft** to merge overlapping TE annotations

```sh
repeatcraft.py -r GCA_927717585.1.contig_named.fasta.out.gff -u GCA_927717585.1.contig_named.fasta.out -c repeatcraft.cgf -o dm101-repeatcraft-A
```


In addition to TE annotation, we have also classified them using the following script : 

```sh
python3 ../src/get_TE_classification_from_OC_output.py dm101-repeatcraft-A.rmerge.gtf dm101-repeatcraft-A-TEclassification.tsv
```


## TE annotation (old version, corresponding  to https://www.biorxiv.org/content/10.1101/2023.05.27.542554 version 2)

We used **RepeatMasker**  to annotate the dmgoth101 assembly with Dfam. Then, the **OneCode** procedure (OneCodeToFindThemAll) was also used to merge LTR and internal parts of TE in one unique feature. The concatenation of the annotations for each contig produced the global annotation. 

We have converted this annotation to GTF format using this script :

```sh
python3 ../src/onecode_output_to_gtf.py Dm_Goth_10-1_one_code.v6.csv dmgoth101.onecode.v6.gtf
```

In addition to TE annotation, we have also classified them using the following script : 

```sh
python3 ../src/get_TE_classification_from_OC_output.py Dm_Goth_10-1_one_code.v6.csv TE_classification.from_OC_output.v6.csv
```
