# Results

This document will explain the organisation of the outputs of the article 10.1101/2023.05.27.542554..

For clarification, output files will be named after input files **FC29** and **FC30**, with **FC29** designating **testis** data and **FC30** **ovaries** data.

### FC[29/30].table.tsv

Each file represents the count table for **FC29** and **FC30** respectively. These counts are made at the copy-level.

- `Subclass` : TE subclass
- `Superfamily` : TE superfamily
- `Family` : TE family
- `Insertion` : insertion ID
- `Unique_Counts` : number of reads assigned to this insertion from uniquely-mapped reads
- `Multi-mapped_Counts` : number of reads potentially assigned to this insertion from multi-mapped reads
- `Mean_Non_Overlapping_Bases` : mean number of non-overlapping bases between the TE copy and the read (for uniquely-mapped reads)
- `Mean_Bases_Outside_TE_Annotation` : mean number of read bases that align beyond the annotated TE boundaries (for uniquely mapped reads)
- `Mean_percent_ofbase_inside_TE` : mean percentage of mapped read length which align within the annotated TE boundaries
- `Mean_TE_Span` : mean percentage of TE length covered by reads. Introns are counted as covered.
- `Mean_soft_clips` : mean number of soft-clipped bases for reads assigned to the insertion
- `Mean_Subcov` : Mean subcoverage (for uniquely mapped reads). Mean percentage of TE length covered by reads. Introns are NOT counted as covered.


### FC[29/30].table.undef.tsv

Each file represents the count table for **FC29** and **FC30** respectively. These counts are made by family, superfamily, or subclass (depends of the lower class available) with multi-mapped reads.

- `Subclass` : TE subclass
- `Superfamily` : TE superfamily
- `Family` : TE family
- `Insertion` : insertion ID
- `Counting` : number of reads assigned to this subclass/superfamily/family


**Please note :** A line with `undef` written in the subclass column represents the count of reads that align to at least one TE and at least one gene. 

 **Please note :** A line with `Unknown_TE` written in the subclass column represents the count of reads that align to TEs only, but have no knwon subclass.


### FC[29/30].table.log

The log file contains counters for every previous file to get an overview of the data.

- Count for uniquely mapped reads
    - `Number of reads that cover a TE locus` : number of reads that cover at least 1 TE locus
    - `Not coverered by 10% of a TE` : number of reads that cover at least 1 TE locus BUT none with at least 10% of coverage of the insertion
    - `Assign to a gene` : kept reads assigned to a gene
    - `Assign to a TE` : kept reads assigned to a TE
    - `Read overlap an exon` : kept reads overlapping an exon (at least 1 base)
    - `Read overlap a gene but not an exon (intronic)` : kept reads overlapping a gene but not an exon
    - `Read don't overlap a gene` : kept reads not overlapping a gene
    - `The TE was the only candidate for a read` : reads assigned to a TE and it don't overlap an other TE
    - `Multiple TE overlap a read` : reads assigned to a TE among others


- Count for multi-mapping reads (with at least 1 alignment which overlap a TE locus)
    - `No alignment assigned to a TE` : reads without any alignment assigned to a TE
    - `Some alignment assigned to a TE` : reads with some alignment assigned to a TE, and some to a gene or nothing
    - `All alignment assigned to a TE` : reads with all alignment assigned to a TE
    - `All alignment assigned to the same TE subclass` : reads with all alignment assigned to the same TE subclass
    - `All alignment assigned to the same TE superfamily` : reads with all alignment assigned to the same TE superfamily
    - `All alignment assigned to the same TE family` : reads with all alignment assigned to the same TE family


### merge.table.csv

This convenient table show a summary of all above file, with count for **FC29** and **FC30** for each family.


- `Subclass` : TE subclass
- `Superfamily` : TE superfamily
- `Family` : TE family
- `nb_of_insertion` : number of insertion in this family
- `Min_TE_length` : minimum length for a TE in this family
- `Max_TE_length` : maximum length for a TE in this family
- `Counts (unique)_female` : count of uniquely-mapped reads assigned to this family in `FC30.table.tsv`
- `nb_of_expressed_insertion_female` : number of insertion with a least 1 count in the female table (FC30)
- `number_of_potential_additional_expressed_insertions_female` : number of insertion with 0 count in the female table but at least 1 in the multi-mapping column in the female table (FC30)
- `Most_expressed_insertion_female` : expression with the maximum count in the female table (FC30)
- `Counts (multi)_female` : count of reads multi-mapped reads assigned to this family in `FC30.table.undef.tsv`
- `Counts (unique+multi)_female` : count of reads assigned to this family in the female table (FC30). It is the sum of the count for all insertions of a family (`Counting` in `FC30.table.tsv`) and the count of the ambiguous read where the insertion is unknown BUT the family is known (`Counting` in `FC30.table.undef.tsv`)
- `Counts (unique)_male` : count of uniquely-mapped reads assigned to this family in `FC29.table.tsv`
- `nb_of_expressed_insertion_male` : number of insertion with a least 1 count in the male table (FC29)
- `number_of_potential_additional_expressed_insertions_male` : number of insertion with 0 count in the male table but at least 1 in the multi-mapping column in the male table (FC29)
- `Most_expressed_insertion_male` : expression with the maximum count in the male table (FC29)
- `Counts (multi)_female` : count of reads multi-mapped reads assigned to this family in `FC29.table.undef.tsv`
- `Counts (unique+multi)_male` : count of reads assigned to this family in the male table (FC29). It is the sum of the count for all insertions of a family (`Counting` in `FC29.table.tsv`) and the count of the ambiguous read where the insertion is unknown BUT the family is known (`Counting` in `FC29.table.undef.tsv`)
