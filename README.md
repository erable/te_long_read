# Transposable element transcription analysis in *D. melanogaster* gonads using Nanopore long reads
This repository is intended to reproduce the results of the article 10.1101/2023.05.27.542554.
 
This repository is organized as follows:
- `./data` contains data or information on how to obtain it
- `./src` contains scripts to run the analysis
- `./res` contains the final results of the analysis


### Dependencies

- minimap2==2.26
- samtools==1.18
- python3>=3.10.12

Also, the necessary python packages are present in [`requirements.txt`](requirements.txt).

**Please note** : You can install all packages at once in a virtual environment (see [Installation](#installation)). 

### Help

This `README.md` will guide you to run the TE analysis performed in the article 10.1101/2023.05.27.542554. You can obtain more help about the scripts by invoking them with the `-h` option.  \
In addition, you can contact `vincent.lacroix@univ-lyon1.fr` or `cristina.vieira@univ-lyon1.fr` for further information.

## Summary

1. [Installation](#installation)
2. [Prepare BAM files (alignment)](#prepare-bam-files-alignment)
    - [Align reads on the reference genome](#align-reads-on-the-reference-genome)
    - [Extract ambiguous reads with multiple max-AS position](#extract-ambiguous-reads-with-multiple-max-as-position)
    - [Sort & Index bam file](#sort--index-bam-file)
3. [Generate supplementary tables](#generate-supplementary-tables)


## Installation

First, clone the project in the folder of your choice :

```bash
git clone https://gitlab.inria.fr/erable/te_long_read.git
```

Then set your current directory (with `cd`) as the new folder and install the dependencies in [requirements.txt](requirements.txt). You can also use a virtual environnement to avoid the conflicts with your existing installation:

```bash
python3 -m venv env # create the virtual env
source env/bin/activate # activate it
pip install -r requirements.txt # or pip3 install -r requirements.txt depending on your installation to install the dependencies
```

**Please note :** All necessary input data files, except the reads (.fastq and .bam), are available in this Git repository. 
If you want information about how to download reads or how other data files were obtained, please refer to [`data/Data_Information.md`](data/Data_Information.md).

## Prepare BAM files (alignment)

### Align reads on the reference genome

Minimap2 has been chosen to align long-reads to the reference genome.

```sh
minimap2 -ax splice --junc-bed data/Dm_Goth_10-1.dmel6.23LiftOff.sorted.noCDS.bed data/GCA_927717585.1.contig_named.fasta data/FC29.fastq.gz > data/FC29.sam

minimap2 -ax splice --junc-bed data/Dm_Goth_10-1.dmel6.23LiftOff.sorted.noCDS.bed data/GCA_927717585.1.contig_named.fasta data/FC30.fastq.gz > data/FC30.sam
```

***Alternative step** : This previous command was performed for 10.1101/2023.05.27.542554, but the following steps would still work if skipped. \
However, the use of a gene annotation (gtf or bed) is optional. You can also run the following command without a GTF.*

```sh
minimap2 -ax splice data/GCA_927717585.1.contig_named.fasta FC29.fastq.gz > data/FC29.sam

minimap2 -ax splice data/GCA_927717585.1.contig_named.fasta FC30.fastq.gz > data/FC30.sam
```

Files **FC29.sam** and **FC30.sam** are generated.

### Extract ambiguous reads with multiple max-AS position

Some reads map to multiple locations of the genome. To mitigate this problem, one strategy consists in keeping only the primary alignment. There are two issues however with this strategy.

First the primary alignment is not always the alignment with the max alignment score (AS tag). Indeed, minimap2 marks seed chains as primary or secondary early in its algorithm, and it may happen, although rarely, that a chain initially marked as secondary yields an alignment with a higher score (https://github.com/lh3/minimap2#algo).
Second, and most importantly, the best alignment can be non unique. In this case, the position of the read in the genome is ambiguous and needs to be handled differently.

The strategy we adopt is the following: if a read maps to several locations, but with one of them having a higher score, we assign the read to the max-AS position.
If a read maps to several locations, but with reads having the same highest score, we store the alignments in a separate file.
Later in the processing, reads from this file are never attributed to a specific insertion, but can be assigned to a TE family.


```sh
python3 ./src/filter_bam.py ./data/FC29.sam ./data/FC29.max_AS.bam ./data/FC29.ambiguous_max_AS.bam
python3 ./src/filter_bam.py ./data/FC30.sam ./data/FC30.max_AS.bam ./data/FC30.ambiguous_max_AS.bam
```

### Sort & Index bam file

This step is required for the next step. We recommend IGV vizualisation of the data using the BAMs produced.

```sh
samtools sort ./data/FC29.max_AS.bam > ./data/FC29.max_AS.sorted.bam
samtools sort ./data/FC29.ambiguous_max_AS.bam > ./data/FC29.ambiguous_max_AS.sorted.bam
samtools index ./data/FC29.max_AS.sorted.bam
samtools index ./data/FC29.ambiguous_max_AS.sorted.bam
```

```sh
samtools sort ./data/FC30.max_AS.bam > ./data/FC30.max_AS.sorted.bam
samtools sort ./data/FC30.ambiguous_max_AS.bam > ./data/FC30.ambiguous_max_AS.sorted.bam
samtools index ./data/FC30.max_AS.sorted.bam
samtools index ./data/FC30.ambiguous_max_AS.sorted.bam
```

## Generate supplementary tables :

First, you should read the README.md in data to get and prepare the dataset.

To generate the counting table for testes <!--(tab TableS3_testes of the file SupTablesNanopore) :-->
use  :
```bash
 ./src/generate_counts.py data/FC29.max_AS.sorted.bam data/FC29.ambiguous_max_AS.sorted.bam data/Dm_Goth_10-1.dmel6.23LiftOff.sorted.gff data/dm101-repeatcraft-A-TEclassification.tsv data/dm101-repeatcraft-A.rmerge.gtf res/FC29.table
```

To generate the counting table for ovaries <!--tab TableS4_ovaries of the file SupTablesNanopore) :-->
use  :
```bash
 ./src/generate_counts.py data/FC30.max_AS.sorted.bam data/FC30.ambiguous_max_AS.sorted.bam data/Dm_Goth_10-1.dmel6.23LiftOff.sorted.gff data/dm101-repeatcraft-A-TEclassification.tsv data/dm101-repeatcraft-A.rmerge.gtf res/FC30.table
```


To generate the summary table for testes and ovaries use :

```bash
./src/merge_coutings.py res/FC29.table.tsv res/FC30.table.tsv res/FC29.table.undef.tsv res/FC30.table.undef.tsv res/merge.table.tsv
```
