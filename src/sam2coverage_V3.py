#!/usr/bin/env python3
import pysam
import sys

"""
usage : sam2coverage <SamFile>
retourne : read_name    subject_name    query_cover     subject_cover

"""


Sam_File = sys.argv[1]
Sam = pysam.AlignmentFile(Sam_File)



for ali in Sam:
    #if (1==1):
    if not (ali.is_secondary or ali.is_unmapped or ali.is_supplementary):
        alignement_len = sum(i[1]-i[0] for i in ali.get_blocks())
	#print ali
	#print alignement_len
        if ali.query_length == 0:
            print("warning")
            #print("Attention les tailles des read risquent de ne pas etre correctes", file=sys.stderr)
            length = ali.infer_read_length()
        else:
            length = ali.query_length
        #print(ali.query_length, alignement_len, length, ali.reference_length)
        query_cover = float(alignement_len) / float(length)
	#print(alignement_len / length)
        #print(ali.reference_name)
        #print(Sam.get_tid(ali.reference_name))
        #tid1=Sam.get_tid(ali.reference_name)
        #print(Sam.reference_lengths)
        #print(tid1)
        #print(ali.reference_name, Sam.get_reference_length(ali.reference_name))
        
        subject_cover = float(alignement_len) / float(Sam.get_reference_length(ali.reference_name))
        
        #j=201.0
        #k=50.0
	#print(j/k)
        print("\t".join(map(str,[ali.query_name, ali.reference_name, query_cover, subject_cover, length,alignement_len, ali.reference_length,Sam.get_reference_length(ali.reference_name)])))
        #print("\t".join(map(str,[ali.query_length,alignement_len])))
