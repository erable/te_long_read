/*Map reads to transcriptome and TEs*/
/*How to build a reference file for transcripts and TEs is explained in splicingAnalysis.sh*/
minimap2 -ax map-ont transcriptsalldmgoth101.onecode.v3.fa -t 8 FC29.fastq.gz -o FC29mapped2transcriptsalldmgoth101.onecode.v3.sam
minimap2 -ax map-ont transcriptsalldmgoth101.onecode.v3.fa -t 8 FC30.fastq.gz -o FC30mapped2transcriptsalldmgoth101.onecode.v3.sam

/*For each read, retrieve the primary alignment, and calculate query coverage, subject coverage*/
python3 sam2coverage_V3.py FC30mapped2transcriptsalldmgoth101.onecode.v3.sam > FC30mapped2transcriptsalldmgoth101.onecode.v3.out
python3 sam2coverage_V3.py FC29mapped2transcriptsalldmgoth101.onecode.v3.sam > FC29mapped2transcriptsalldmgoth101.onecode.v3.out

/*In R, plot the breadth of coverage*/
FC29=read.table("FC29mapped2transcriptsalldmgoth101.onecode.v3.out")
FC30=read.table("FC30mapped2transcriptsalldmgoth101.onecode.v3.out")
boxplot(sample(FC30$V4),sample(FC29$V4),axes=F,col=c("purple","green"),main ="Transcript Coverage")
axis(1,at=c(1,2),labels=c("Ovaries","Testes"))
axis(2)
box()
