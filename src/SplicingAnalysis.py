#!/usr/bin/env python3
import pysam
import sys

"""
usage : sam2coverage <SamFile>
retourne : read_name    subject_name    query_cover     subject_cover

"""


Sam_File = sys.argv[1]
Sam = pysam.AlignmentFile(Sam_File)



for ali in Sam:
    if not (ali.reference_name[0:3]=="FBt"):
        
        alignement_len = sum(i[1]-i[0] for i in ali.get_blocks())
        if ali.query_length == 0:
            #print("warning")
            #print("Attention les tailles des read risquent de ne pas etre correctes", file=sys.stderr)
            length = ali.infer_read_length()
        else:
            length = ali.query_length
        #print(ali.query_length, alignement_len, length, ali.reference_length)
        query_cover = float(alignement_len) / float(length)
	#print(alignement_len / length)
        #subject_cover = float(alignement_len) / float(ali.reference_length) 
        subject_cover = float(alignement_len) / float(Sam.get_reference_length(ali.reference_name))
        pos=ali.reference_start
        intronB=0
        intronL=0
        for x in ali.cigartuples:
            if(x[0]==0):
                pos=pos+x[1]#match or mismatch
            if(x[0]==1):
                pos=pos#insertion
            if(x[0]==2):
                pos=pos+x[1] #deletion
            if(x[0]==3):
            	intronB=pos
            	intronL=x[1]
            	pos=pos+intronL #in the case where there are several introns, we take the last one                
        print("\t".join(map(str,[ali.query_name, ali.reference_name, query_cover, subject_cover, length,alignement_len, ali.reference_length, ali.cigarstring.find("N"), intronB, intronL])))

        
        
