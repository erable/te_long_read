#!/usr/bin/env python3
import pysam
import argparse
import pandas as pd
from tqdm import tqdm as tqdm



# Classes

class Gene:
    def __init__(self, chrom, start, end, gene_id):
        self.chrom = chrom
        self.start = start
        self.end = end
        self.gene_id = gene_id
        self.exons = set()
        self.mrna = set()
        self.transcripts = {}

    def __len__(self):
        return self.end - self.start

    def __repr__(self):
        return self.gene_id



class TE_feature:
    def __init__(self, chrom, start, end, insertion_id):
        self.chrom = chrom
        self.start = start
        self.end = end
        self.insertion_id = insertion_id
        self.count = 0
        self.mm_count = 0
        self.counted_reads = set()
        self.chimeric_reads = set()
        self.nob = list()  # non overlapping bases
        self.span = list() 
        self.soft_clips = list() 
        self.bases_outside_te_annot = list()
        self.percentage_inside_TE = list()


        self.gene_nob = list()
        self.best_gene = list()

        self.gene_relative_nob = list()
        self.te_relative_nob = list()


    def __len__(self):
        return self.end - self.start

    def __repr__(self):
        return self.insertion_id

    def is_valid(self, bam_chromosomes, min_length=150):
        return len(self) > min_length and self.chrom in bam_chromosomes
    



# Functions

def parse_gene_annotation_line(line):
    sline = line.strip().split("\t")
    chrom = sline[0]
    start = int(sline[3])
    end = int(sline[4])
    gene_id = sline[-1].split(";")[0].split('"')[1]
    return Gene(chrom, start, end, gene_id)


# def is_overlapped(start1, end1, start2, end2):
#     return end1 >= start2 and end2 >= start1

def get_span(read, insertion):
    start = max( insertion.start ,read.reference_start)
    end = min( insertion.end ,read.reference_end)
    return (end-start)/(insertion.end - insertion.start)

def get_gene_dict(gene_annotation):
    """
    Convert an annotation file (gff) of gene into a dict(key(str) : chromosome)
    of dict (key(str) : gene_ID) of Gene.
    
    Args :
        gene_annotation (str) : path of the gene annotation (GFF format)
    """
    gene_dict = {}
    with open(gene_annotation, 'r') as gene_annot:
        for line in gene_annot:
            sline = line.strip().split('\t')
            gene_id = sline[-1].split(";")[0].split('"')[1]
            transcript_id = sline[-1].split(";")[2].split('"')[1]
            if sline[2] == "gene":
                chrom = sline[0]
                new_gene = parse_gene_annotation_line(line)
                if chrom in gene_dict:
                    gene_dict[chrom][new_gene.gene_id] = new_gene
                else:
                    gene_dict[chrom] = {new_gene.gene_id: new_gene}
            elif sline[2] == "exon":
                gene_dict[chrom][gene_id].exons.add((int(sline[3]), int(sline[4])))
                gene_dict[chrom][gene_id].transcripts.setdefault(transcript_id,[]).append((int(sline[3]), int(sline[4])))

            elif sline[2] == "mRNA":
                gene_dict[chrom][gene_id].mrna.add(
                    (int(sline[3]), int(sline[4])))
    return gene_dict


def build_TE(line):
    sline = line.strip().split("\t")
    chrom = sline[0]
    start = int(sline[3])
    end = int(sline[4])
    gene_id = sline[-1].split(";")[0].split('"')[1]
    insertion_id = sline[-1].strip().split('transcript_id "')[-1][:-2]

    return TE_feature(chrom, start, end, insertion_id)


def filter_relevant_TE_feature(bamfile, TE_annotation_file):
    """
    Generate list of TE objects that will next be counted.
    TE are filtered : we discard those which are on chromosome absent from the bamfile
    and those with length (in number of base) below a certain threshold (150).

    Args:
        bamfile (str): path to the alignment file
        TE_annotation_file (str): path the TE annotation file (gtf format)
    """
    # Enumerating chromosomes present in the bamfile
    bam_chromosomes = pysam.AlignmentFile(bamfile).references
    # Then iterating through TE_annotation_file, creating and checking TE objects
    valid_TE_list = list()
    with open(TE_annotation_file, "r") as TE_annot:
        for line in TE_annot:
            new_TE = build_TE(line)
            if new_TE.is_valid(bam_chromosomes):  # filtering
                valid_TE_list.append(new_TE)
    return valid_TE_list


def regroup_TE_by_chrom(TE_feature_list):
    """
    Return a dict of key:chrom and values:list of TE

    Args:
        TE_feature_list (list): flat list of TE_feature
    """
    TE_dict = dict()
    for insertion in TE_feature_list:
        if insertion.chrom not in TE_dict:
            TE_dict[insertion.chrom] = [insertion]
        else:
            TE_dict[insertion.chrom].append(insertion)
    return TE_dict


def get_all_reads_covering_TE(TE_by_chrom, bamfile):
    """
    Get a list of all reads that cover a TE.

    Args:
        TE_by_chrom (dict): dict of TE_feature ordered by chromosome
        bamfile (str): path to the alignment file
    """
    reads_mapped_on_TE = set()
    with pysam.AlignmentFile(bamfile) as bam:
        for chrom, TE_list in tqdm(TE_by_chrom.items(), desc="Keeping read with TE", position=0, leave=True):
            for TE in tqdm(TE_list, desc=chrom, position=0, leave=True):
                reads_fetched = list(bam.fetch( contig=TE.chrom, start=TE.start, stop=TE.end) )
                # Only keeping reads that are at least aligned once in a TE insertion :
                overlapped_reads = [read for read in reads_fetched if read.get_overlap(TE.start, TE.end)]
                reads_mapped_on_TE.update(overlapped_reads)
    return reads_mapped_on_TE



def get_overlapped_genes(read, gene_dict):
    """
    Takes a read and a dictionnary of dictionnaries (chrom -> gene_id -> [start, end])
     and returns a list of all overlapped genes.

    Args:
        read (pysam.AlignedSegment): the read we want to map
        gene_dict (dict): a dict of dict (gene start/end by gene_id by chrom)

    Returns:
        overlapped_genes_dict (dict) : dict of all genes covered by the read with their start and end position as values
    """
    overlapped_genes = list()
    overlap_intronic = list()
    chrom = read.reference_name
    if chrom in gene_dict.keys():
        gene_list = gene_dict[chrom].values()
        for gene in gene_list:
            overlap_size = sum(read.get_overlap(start,end) for start,end in gene.exons)
            if overlap_size > 1:
                overlapped_genes.append(gene)
            elif read.get_overlap(gene.start,gene.end) > 1:
                overlap_intronic.append(gene)
        # overlapped_genes = [gene for gene in gene_list if is_overlapped(
            # read.reference_start, read.reference_end, gene.start, gene.end)]
    else:
        overlapped_genes = []
    return overlapped_genes, overlap_intronic



def get_overlapped_TE(read, TE_by_chrom, subcov_threshold):
    """
    Get all TE feature that are covered by a read

    Args:
        read (pysam.AlignedSegment): the read we want to map
        TE_by_chrom (dict): dict of TE_feature with key:chrom, value:list of TE
    """
    list_of_overlapped_TE = list()
    chrom = read.reference_name
    for insertion in TE_by_chrom[chrom]:
        # if read.get_overlap(insertion.start, insertion.end) and default_filter(read, insertion, subcov_threshold) :  # if at least 1 base aligned in TE
        overlap  = default_filter(read, insertion, subcov_threshold) 
        if overlap :  # if at least 1 base aligned in TE
            list_of_overlapped_TE.append(insertion)
    return list_of_overlapped_TE


def get_subject_coverage(alignment, insertion):
    """
    Return the percentage of a TE aligned with a read on a alignment.
    
    Args :
        alignment (pysam.AlignedSegment): the alignment
        insertion (TE_feature) : the TE insertion
        
    Return :
        float : the TE coverage
    """
    insertion_length = insertion.end - insertion.start +1
    if insertion_length != 0:
        return alignment.get_overlap(insertion.start,insertion.end)/insertion_length
    else:
        return 0
    

# def get_nb_of_non_overlapping_bases(x_start, x_end, y_start, y_end):
#     # work only if x and y overlap
#     start_overlap = abs(x_start - y_start)
#     end_overlap = abs(x_end - y_end)
#     return start_overlap + end_overlap

# def get_nb_of_bases_outside_te_annot(te_start, te_end, read_start, read_end):
#     if (te_end < read_start) or (read_end < te_start):
#         return (te_end-te_start) + (read_end-read_start)
#     bota = 0
#     if read_start < te_start:
#         bota += te_start-read_start
#     if te_end < read_end:
#         bota += read_end-te_end
#     return bota


def choose_feature_with_less_non_overlapping_bases(read, list_of_TE, list_of_gene):
    # Return TE if TE is the best feature, else 0
    min_nob = 999999999999
    min_nob_gene = 999999999999
    min_relative_nob_TE = 999999999999
    min_relative_nob_Gene = 999999999999
    #optimal_feature = None
    optimal_gene = None 
    optimal_TE = None
    #print(list_of_TE)
    for TE in list_of_TE:
        matched_pairs = read.get_overlap(TE.start, TE.end)
        nb_of_nob = (TE.end - TE.start + 1) + read.query_alignment_length - 2 * matched_pairs   
        #nb_of_nob = get_nb_of_non_overlapping_bases(TE.start, TE.end, read)
        if nb_of_nob < min_nob:
            min_nob = nb_of_nob
            optimal_TE = TE
            min_relative_nob_TE= min_nob / ((TE.end - TE.start + 1) + read.query_alignment_length)
            #optimal_feature = TE
            #print(TE.insertion_id, min_nob)
    for gene in list_of_gene:
        if gene.transcripts:
            for transcript_id, exons in gene.transcripts.items():
                matched_pairs = 0
                
                for exon in exons:
                    matched_pairs += read.get_overlap(exon[0], exon[1])

                    #nb_of_nob += get_nb_of_non_overlapping_bases(exon[0],exon[1], read)
                nb_of_nob_gene = sum(exon[1] - exon[0] +1 for exon in exons) + read.query_alignment_length - 2 * matched_pairs
                if nb_of_nob_gene < min_nob_gene:
                    min_nob_gene = nb_of_nob_gene
                    optimal_gene = gene
                    min_relative_nob_Gene = min_nob_gene / (sum(exon[1] - exon[0] +1 for exon in exons) + read.query_alignment_length)
                    if nb_of_nob_gene < min_nob:
                        return 0, (optimal_TE, min_nob, min_relative_nob_TE), (optimal_gene, min_nob_gene, min_relative_nob_Gene)
        else:
            matched_pairs = read.get_overlap(gene.start, gene.end)
            nb_of_nob_gene = (gene.end - gene.start + 1) + read.query_alignment_length - 2 * matched_pairs
            if nb_of_nob_gene < min_nob_gene:
                min_nob_gene = nb_of_nob_gene
                min_relative_nob_Gene = min_nob_gene / ((gene.end - gene.start + 1) + read.query_alignment_length)

                if nb_of_nob_gene < min_nob:
                        return 0, (optimal_TE, min_nob, min_relative_nob_TE), (optimal_gene, min_nob_gene, min_relative_nob_Gene)
    return optimal_TE, (optimal_TE, min_nob, min_relative_nob_TE ), (optimal_gene if optimal_gene is not None else "NA" , min_nob_gene if min_nob_gene < 999999999999 else "NA", min_relative_nob_Gene if min_relative_nob_Gene < 999999999999 else "NA")


# def check_if_exonic_alignment(read, gene):
#     """
#     Return True if a read has at least 1 base aligned with an exon of a gene.

#     Args:
#         read (pysam.AlignedSegment): a pysam read
#         gene (Gene): a gene

#     Returns:
#         bool: True if aligned in exonic region, else False
#     """
#     exonic_positions=sorted(list(gene.exons))
#     for exonStart, exonEnd in exonic_positions:
#         if read.reference_end >= exonStart and exonEnd >= read.reference_start:
#             return True
#     return False

def default_filter(alignment, insertion, subcov_threshold):
    """
    Return True if 
        the subject coverage is > threshold
    AND at least 1 base of the alignment is aligned
    else return False
    
    Args:
        alignment (pysam.AlignedSegment): a pysam alignment
        insertion (TE_feature): a TE

    Returns:
        bool: True if the filter is passed 
    """
    #subject_coverage=get_subject_coverage(alignment, insertion)
    nb_aligned_pairs=alignment.get_overlap(insertion.start, insertion.end)
    subject_coverage = nb_aligned_pairs / (insertion.end - insertion.start +1) 
    is_ok=(subject_coverage > subcov_threshold and nb_aligned_pairs > 1)
    is_ok_test=(subject_coverage > subcov_threshold and nb_aligned_pairs >= 1)
    if is_ok_test and not is_ok:
        print(alignment.to_string())
        print(alignment.reference_start, alignment.reference_end)
        print(insertion)
        print(subject_coverage, subcov_threshold)
        print(nb_aligned_pairs)
    # if insertion.insertion_id == "Copia$3L_RaGOO$10022428$10027467":
    #     print("is_valid :" , is_ok)
    return is_ok


def increment_TE_count(TE_by_chrom, reads_list, gene_dict):  # TODO : fix Args !
    """
    Iterate through each read that covers a TE and decide if we increment (or not)
    its read counter. This decision depends on several conditions described in the filter function.
    Update a dict of TE AND return counters

    Args:
        TE_by_chrom (dict): dict of TE_feature with key:chrom, value:list of TE
        reads_list (list): list of all the reads that cover a TE locus
        gene_annotation (dict): dict of Gene_feature with key:chrom, value:list of TE
    """
    subcov_threshold = MIN_SUB_COVERAGE
    not_covered_by_min_subcov = 0
    best_aligned_on_gene = 0
    exonic = 0
    intronic = 0
    intergenic = 0
    multiple_TE = 0
    non_ambiguous = 0
    total_read = len(reads_list)
    for read in tqdm(reads_list, position=0, leave=True):
        # (1) Fetching TE insertions that are at least covered by one base.
        # overlapped_TE = get_overlapped_TE(read, TE_by_chrom)
        overlapped_TE=get_overlapped_TE(read, TE_by_chrom,subcov_threshold)
        #print(overlapped_TE)
        #print("is_included :", "Copia$3L_RaGOO$10022428$10027467" in [i.insertion_id for i in overlapped_TE])

        # (2) filtering TE that are not covered by at least 10% of their length and at least 1 aligned base
        # overlapped_TE=[TE for TE in overlapped_TE if default_filter(read, TE, subcov_threshold)]
        if not overlapped_TE :
            not_covered_by_min_subcov += 1
            continue

        # (3) filtering feature with the less non-overlapping bases
        overlapped_in_gene_exons, overlap_in_intron_only = get_overlapped_genes(read, gene_dict)
        # overlapped_in_gene_exons=[gene for gene in overlapped_genes if check_if_exonic_alignment(read, gene)]

        chosen_feature, (optimal_TE, min_nob, min_relative_nob_TE), (optimal_gene, min_nob_gene, min_relative_nob_Gene)=choose_feature_with_less_non_overlapping_bases(read, overlapped_TE, overlapped_in_gene_exons)
        
        if isinstance(chosen_feature, TE_feature):
            #print(chosen_feature.insertion_id)
            chosen_feature.count += 1
            chosen_feature.counted_reads.add(read)
            bota =  read.query_alignment_length - read.get_overlap(chosen_feature.start, chosen_feature.end)#   get_nb_of_bases_outside_te_annot(chosen_feature.start, chosen_feature.end, read.reference_start, read.reference_end)
            percentage_of_read_insideTE = 1 - (bota /read.query_alignment_length)
            #print(read)
            #print(read.get_cigar_stats())
            #print(read.get_cigar_stats()[0])
            #print(read.query_length)
            #print(read.infer_query_length())
            print(read.get_cigar_stats()[0][4])
            #percent_soft_clip = read.get_cigar_stats()[0][4]/read.query_length
            #We need to use infer_query_length because in case we use max_AS hits that are not primary, the read.query_length is 0
            percent_soft_clip = read.get_cigar_stats()[0][4]/read.infer_query_length()

            chosen_feature.nob.append(min_nob)
            chosen_feature.span.append(get_span(read,chosen_feature))
            chosen_feature.soft_clips.append(percent_soft_clip)


            chosen_feature.percentage_inside_TE.append(percentage_of_read_insideTE)
            chosen_feature.bases_outside_te_annot.append(bota)

            if min_nob_gene != "NA":
                chosen_feature.gene_nob.append(min_nob_gene)
                chosen_feature.gene_relative_nob.append(min_relative_nob_Gene)
                chosen_feature.te_relative_nob.append(min_relative_nob_TE)

            if optimal_gene!= "NA":
                chosen_feature.best_gene.append(optimal_gene.gene_id)




            if overlapped_in_gene_exons :
                exonic += 1
            elif overlap_in_intron_only :
                intronic += 1
            else :
                intergenic += 1

            if len(overlapped_TE) > 1:
                multiple_TE += 1
            else :
                non_ambiguous += 1

        else :
            best_aligned_on_gene += 1
    counters = {"Number of reads that cover a TE locus : ": total_read,
                "Not coverered by 10% of a TE : ": not_covered_by_min_subcov,
                "Assigned to a gene : ": best_aligned_on_gene,
                "Assigned to a TE : " : exonic + intronic + intergenic,
                "Read overlap an exon : " : exonic,
                "Read overlap a gene but not an exon (intronic) : " : intronic,
                "Read don't overlap a gene : " : intergenic,
                "The TE was the only candidate for a read : " : non_ambiguous,
                "Multiple TE overlap a read : " : multiple_TE,
                }
    return counters


def generate_counting(bamfile, TE_annotation_file, gene_annotation_file):
    """
    Measure the expression of each TE by number of mapped reads.
    Return a dict with key = insertion_name and value = a list of mapped read

    Args:
        bamfile (str): path to the alignment file
        TE_annotation_file (str): path to the TE annotation file in gtf format
        gene_annotation_file (str): path to the gene annotation file in gtf format
        TE_classification_file (str): path to a tsv table with TE classification (family superfamily, subclass)
    """
    print("Filtering valid TE...")
    gene_dict=get_gene_dict(gene_annotation_file)
    filtered_TE_features=filter_relevant_TE_feature(bamfile, TE_annotation_file)
    TE_by_chrom=regroup_TE_by_chrom(filtered_TE_features)
    print("Recovering reads covering TE...")
    reads_covering_TE=get_all_reads_covering_TE(TE_by_chrom, bamfile)
    print("Done")
    print("Incrementing TE counts...")
    counters_results=increment_TE_count(TE_by_chrom, reads_covering_TE,  gene_dict)
    print("Done")
    return [TE_by_chrom, counters_results]

def from_raw_counting_to_df(TE_dict, TE_classification_file):
    """
    Convert dict of counting generated by func "generate_counting" into a more user-friendly dataframe.

    Args:
        TE_dict (dict): dictionnary of every TE insertion by chromosome with counting and all...
        TE_classification_file (str): path to a tsv table with TE classification (family superfamily, subclass)

    Returns:
        pd.DataFrame: a pandas dataframe containing informations about each TE insertions (ID, counting, reads counted...)
    """
    TE_classification_df=pd.read_csv(TE_classification_file, sep="\t", names=[
                                     "Subclass", "Superfamily", "Family", "Insertion"])

    flat_insertion_list=[]
    for insertion_list in TE_dict.values():
        flat_insertion_list += insertion_list
    insertion_dict=dict(zip([insertion.insertion_id for insertion in flat_insertion_list], [
                        insertion for insertion in flat_insertion_list]))
    counting_list=[insertion.count for insertion in flat_insertion_list]
    mm_counting_list=[insertion.mm_count for insertion in flat_insertion_list]
    counted_read_list=[
        insertion.counted_reads for insertion in flat_insertion_list]
    mean_nob_read_list = [sum(insertion.nob)/len(insertion.nob) if len(insertion.nob) > 0 else "" for insertion in flat_insertion_list]
    mean_bota_read_list = [sum(insertion.bases_outside_te_annot)/len(insertion.bases_outside_te_annot) if len(insertion.bases_outside_te_annot) > 0 else "" for insertion in flat_insertion_list]
    mean_base_inside_TE = [sum(insertion.percentage_inside_TE)/len(insertion.percentage_inside_TE) if len(insertion.percentage_inside_TE) > 0 else "" for insertion in flat_insertion_list]
    mean_span = [sum(insertion.span)/len(insertion.span) if len(insertion.span) > 0 else "" for insertion in flat_insertion_list]
    mean_soft_clips = [sum(insertion.soft_clips)/len(insertion.soft_clips) if len(insertion.soft_clips) > 0 else "" for insertion in flat_insertion_list]
    mean_gene_nobs = [sum(insertion.gene_nob)/len(insertion.gene_nob) if len(insertion.gene_nob) > 0 else "" for insertion in flat_insertion_list]
    best_genes_len = [len(insertion.best_gene) for insertion in flat_insertion_list]
    best_genes = [ ", ".join([f"{i}:{insertion.best_gene.count(i)}" for i in set(insertion.best_gene)]) for insertion in flat_insertion_list]
    mean_gene_relative_nobs = [sum(insertion.gene_relative_nob)/len(insertion.gene_relative_nob) if len(insertion.gene_relative_nob) > 0 else "" for insertion in flat_insertion_list]
    mean_te_relative_nobs = [sum(insertion.te_relative_nob)/len(insertion.te_relative_nob) if len(insertion.te_relative_nob) > 0 else "" for insertion in flat_insertion_list]


    counting_df=pd.DataFrame(list(zip(insertion_dict.keys(), counting_list, mm_counting_list, counted_read_list, mean_nob_read_list, mean_bota_read_list,mean_base_inside_TE,mean_span, mean_soft_clips, mean_gene_nobs, best_genes_len, best_genes, mean_te_relative_nobs, mean_gene_relative_nobs)),
                columns=['Insertion', 'Unique_Counts', 'Multi-mapped_Counts', 'Reads', "Mean_Non_Overlapping_Bases", "Mean_Bases_Outside_TE_Annotation","Mean_percent_ofbase_inside_TE","Mean_TE_Span","Mean_soft_clips", "Mean_Gene_Non_Overlapping_Bases", "Number_Of_Reads_Overlapping_Genes_And_TEs", "Overlapped_Genes", "%Non_Overlapping_Bases_TE(for_reads_overlapping_genes_and_TE)","%Non_Overlapping_Bases_Gene(for_reads_overlapping_genes_and_TE)"  ])
    counting_df=TE_classification_df.merge(counting_df, on="Insertion")
    add_mean_subject_coverage_to_df(counting_df, insertion_dict)
    counting_df.sort_values(by='Unique_Counts',ascending=False)
    return counting_df

def get_insertion_mean_subject_coverage(insertion_id, counted_reads, insertion_dict):

    insertion=insertion_dict[insertion_id]
    mean_subject_coverage=0
    if len(counted_reads) == 0:
        return 0
    for read in counted_reads:
        mean_subject_coverage += get_subject_coverage(read, insertion)
    return mean_subject_coverage/len(counted_reads)

def add_mean_subject_coverage_to_df(counting_df, insertion_dict):
    counting_df["Mean_Subcov"]=counting_df.apply(lambda x: get_insertion_mean_subject_coverage(
        x['Insertion'], x['Reads'], insertion_dict), axis=1)
    return counting_df

def generate_TE_df(bamfile, TE_gtf, gene_gtf, TE_classification_file):
    TE_by_chrom, counters_results = generate_counting(bamfile, TE_gtf, gene_gtf)
    #print(counters_results)
    big_TE_df = from_raw_counting_to_df(TE_by_chrom, TE_classification_file)
    return [big_TE_df, counters_results]

def save_counting_df(counting_df, csv_file):
    counting_df.to_csv(csv_file, sep = '\t', index=False)

# Classes and functions to manage ambiguous read

class TE_class:
    """
    Represent an insertion with his classification.
    """

    def __init__(self, subclass, superfamily, family, insertion):
        self.subclass = subclass
        self.superfamily = superfamily
        self.family = family
        self.insertion = insertion

    def __repr__(self):
        return self.subclass + "|" + self.superfamily + "|" + self.family + "|" + self.insertion


def make_te_classification_structure(TE_classification_file):
    """
    Return a dict with an insertion ID as keys and a TE_class object as values.
    
    Args :
        TE_classification_file : path of the TE_classification_file
        
    Return :
        dict(str, TE_class) : dict with an insertion ID as keys and a TE_class object as values"""
    TE_classification_df=pd.read_csv(TE_classification_file, sep="\t", names=[
                                     "Subclass", "Superfamily", "Family", "Insertion"])
    TE_struct = dict()
    TE_struct.update([(row[-1],TE_class(row[0], row[1], row[2], row[3])) for row in TE_classification_df[["Subclass", "Superfamily", "Family", "Insertion"]].to_numpy()])
    return TE_struct

def get_rname_covering_TE(ali_covering_TE):
    """
    Return a set of all read name where at least 1 of their alignment cover a TE.
    
    Args :
        ali_covering_TE (pysam.AlignmentSegment) : list of the alignment covering a TE

    Returns :
        reads_covering_TE set(str): set of name reads
    """
    reads_covering_TE = set()
    for ali in ali_covering_TE:
        reads_covering_TE.add(ali.query_name)
    return reads_covering_TE    

def complete_TE_count(TE_count, alignments, subclass, superfamily, family):
    """
    In the TE_count dictionnary, increase by 1 the correct TE counter and add counted reads.

    Args :
        TE_count (dict) : dict of TE_feature sorted by class
        alignments (list(pysam.AlignmentSegment)) : list of alignment from a read of this class
        family (str) : family of the TE
        superfamily (str) : superfamily of the TE
        subclass (str) : subclass of the TE
    """
    if subclass in TE_count:
        if superfamily in TE_count[subclass]:
            if family in TE_count[subclass][superfamily]:
                TE_count[subclass][superfamily][family][0] += 1
                TE_count[subclass][superfamily][family][1].extend(alignments)
            else:
                TE_count[subclass][superfamily][family] = [1, alignments]
        else:
            TE_count[subclass][superfamily] = {family : [1, alignments]}
    else:
        TE_count[subclass] = {superfamily : {family : [1, alignments]} }

def increment_TE_amb_count(TE_count, TE_struct, alignments, gene_dict, TE_by_chrom, counter):
    """
    Fill TE_count to represent the ambiguous data..
    
    Args :
        TE_count (dict(dict(dict(list)))) : information gather for each family of TE for ambiguous read
        TE_scrupt (dict) : represent the structure of TE family
        alignments (list) : list of alignment for a read
        gene_dict (dict) : dictionary of gene
        TE_by_chrom (dict) : representation by chromosome of the insertion
        counter (dict) : some values increase during the program 
    """
    subcov_threshold = MIN_SUB_COVERAGE
    results = []
    for ali in alignments:
        # (1) Fetching TE insertions that are at least covered by one base.
        overlapped_TE=get_overlapped_TE(ali, TE_by_chrom,subcov_threshold)

        # overlapped_TE=get_overlapped_TE(ali, TE_by_chrom)
        # (2) filtering TE that are not covered by at least 10% of their length and at least 1 aligned base
        # overlapped_TE=[TE for TE in overlapped_TE if default_filter(ali, TE, subcov_threshold)]
        if not overlapped_TE :
            results.append(0)
            continue
        # (3) filtering feature with the less non-overlapping bases
        # overlapped_genes=get_overlapped_genes(ali, gene_dict)
        # overlapped_in_gene_exons=[gene for gene in overlapped_genes if check_if_exonic_alignment(ali, gene)]
        overlapped_in_gene_exons, overlap_in_intron_only = get_overlapped_genes(ali, gene_dict)
        
        chosen_feature,  (optimal_TE, min_nob, min_relative_nob_TE), (optimal_gene, min_nob_gene, min_relative_nob_Gene) = choose_feature_with_less_non_overlapping_bases(ali, overlapped_TE, overlapped_in_gene_exons)
        results.append(chosen_feature)

    results = list(set(results))


    if 0 in results and len(results) == 1:  # reads s'alignent nul part
        counter["No alignment assigned to a TE : "]+=1
    else:
        for res in results:
            if res != 0:
                res.mm_count += 1
        if 0 in results:
            TE_count["undef"]["undef"]["undef"][0] += 1
            TE_count["undef"]["undef"]["undef"][1].extend(alignments)
            counter["Some alignment assigned to a TE : "] += 1
        else:
            counter["All alignment assigned to a TE : "] += 1
            family = TE_struct[results[0].insertion_id].family
            superfamily = TE_struct[results[0].insertion_id].superfamily
            subclass = TE_struct[results[0].insertion_id].subclass
            if all([TE_struct[res.insertion_id].family == family for res in results]):
                complete_TE_count(TE_count, alignments, subclass=subclass, superfamily=superfamily, family=family)
                counter["All alignment assigned to the same TE subclass : "] += 1
                counter["All alignment assigned to the same TE superfamily : "] += 1
                counter["All alignment assigned to the same TE family : "] += 1
            elif all([TE_struct[res.insertion_id].superfamily == superfamily for res in results]):
                complete_TE_count(TE_count, alignments, subclass=subclass, superfamily=superfamily, family="undef")
                counter["All alignment assigned to the same TE subclass : "] += 1
                counter["All alignment assigned to the same TE superfamily : "] += 1
            elif all([TE_struct[res.insertion_id].subclass == subclass for res in results]):
                complete_TE_count(TE_count, alignments, subclass=subclass, superfamily="undef", family="undef")
                counter["All alignment assigned to the same TE subclass : "] += 1
            else:
                complete_TE_count(TE_count, alignments, subclass="Unknown_TE", superfamily="undef", family="undef")

def count_ambiguous(BAM_UNDEF_FILE, TE_by_chrom, GENE_ANNOTATIONS_FILE, TE_struct):
    """
    Find, for each ambiguous read, its assignation.
    
    Args :
        BAM_UNDEF_FILE (str) : path of the bam undef file
        TE_by_chrom (dict) : TE_feature sorted on a dictionnary by chromosome (key)
        GENE_ANNOTATIONS_FILE (str) : path of the gene annotation file
        TE_struct (dict) : TE_class aviable with an insertion id (key)
    """
    ali_covering_TE=get_all_reads_covering_TE(TE_by_chrom, BAM_UNDEF_FILE)  # return ALI covering TE
    reads_covering_TE =get_rname_covering_TE(ali_covering_TE)
    gene_dict=get_gene_dict(GENE_ANNOTATIONS_FILE)
    TE_count={"undef" : {"undef" : {"undef" : [0, []]}}}
    counters={"No alignment assigned to a TE : ":0,
             "Some alignment assigned to a TE : ":0,
             "All alignment assigned to a TE : ":0,
             "All alignment assigned to the same TE subclass : ":0,
             "All alignment assigned to the same TE superfamily : ":0,
             "All alignment assigned to the same TE family : ":0}
    with pysam.AlignmentFile(BAM_UNDEF_FILE, "r") as bam:
        index = pysam.IndexedReads(bam)
        index.build()
        for read in tqdm(reads_covering_TE, position=0, leave=True):
            alignments = list(index.find(read))
            increment_TE_amb_count(TE_count, TE_struct, alignments, gene_dict, TE_by_chrom, counters)
    
    return TE_count, counters

def convert_TE_count_to_df(TE_count):
    """
    Convert the count of ambiguous TE in a dictionary to a pandas dataframe.
    
    Args :
        TE_count (dict(dict(dict(list)))) : information gather for each family of TE for ambiguous read
        
    Return :
        df (pd.Dataframe) : dataframe of ambiguous count
    """
    df = pd.DataFrame(columns=["Subclass","Superfamily","Family","Insertion","Counting","Reads"])
    for subclass in TE_count:
        for superfamily in TE_count[subclass]:
            for family in TE_count[subclass][superfamily]:
                insertion = "undef"
                counting = TE_count[subclass][superfamily][family][0]
                reads = TE_count[subclass][superfamily][family][1]
                row = pd.Series({"Subclass":subclass,"Superfamily":superfamily,"Family":family,"Insertion":insertion,"Counting":counting,"Reads":reads})
                df = pd.concat([df, row.to_frame().T], ignore_index=True)      
    return df


def generate_undef_df(BAM_UNDEF_FILE, TE_by_chrom, GENE_ANNOTATIONS_FILE, TE_CLASSIFICATION_FILE):
    """Generate a dataframe with information about expressed TE in unknown location.

    Args :
        BAM_UNDEF_FILE (str) : path of the bam with ambiguous alignment
        RM_TE_ANNOTATION_FILE (str) : path of the TE annotation (gtf)
        GENE_ANNOTATION_FILE (str) : path of the gene annotation (gtf)

    Returns :
        df (pd.Dataframe) : dataframe of ambiguous count
        counter (dict) : return some metrics counted during the program
    """
    TE_struct = make_te_classification_structure(TE_CLASSIFICATION_FILE)
    TE_count, counter = count_ambiguous(BAM_UNDEF_FILE, TE_by_chrom, GENE_ANNOTATIONS_FILE, TE_struct)
    df = convert_TE_count_to_df(TE_count)
    return df, counter

def print_log(COUNTING_FILE, count_uniq, count_amb=None):
    """
    Print in a log file some counting.
    """
    with open(COUNTING_FILE, "w") as log_file:
        log_file.write("For unique-mapping read :\n")
        for key,value in count_uniq.items():
            log_file.write(key + str(value) + "\n")
        if count_amb:
            log_file.write("\n\nFor multi-mapping read :\n")
            for key,value in count_amb.items():
                log_file.write(key + str(value) + "\n")




if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('BAM_FILE', type=str, help='A BAM file containing only primary alignements.')
    parser.add_argument('BAM_UNDEF_FILE', type=str, help='A BAM file containing undefined read alignment.')
    parser.add_argument('GENE_ANNOTATIONS_FILE', type=str, help='Genes annotation file in gtf or gff format.')
    parser.add_argument('TE_CLASSIFICATION_FILE', type=str, help='Classification of transposable elements in a tsv format.')
    parser.add_argument('RM_TE_ANNOTATION_FILE', type=str, help='Transposable elements annotation file in gtf or gff format.')
    parser.add_argument('OUTPUT_PREFIX', type=str, help='Prefix of output file as tsv format.')
    
    parser.add_argument('-c','--min-sub-coverage', type=float, help="Threshold used to filter the features (gene or TE) mapped by a read = minimal subject coverage (nb of aligned bases / total nb of feature's bases)",default=0.1)

    args = parser.parse_args()



    # GLOBAL VARS
    BAM_FILE = args.BAM_FILE# sorted by position and indexed
    BAM_UNDEF_FILE = args.BAM_UNDEF_FILE # sorted by read name

    GENE_ANNOTATIONS_FILE = args.GENE_ANNOTATIONS_FILE 

    TE_CLASSIFICATION_FILE = args.TE_CLASSIFICATION_FILE 

    RM_TE_ANNOTATION_FILE = args.RM_TE_ANNOTATION_FILE 

    OUTPUT_PREFIX = args.OUTPUT_PREFIX 

    MIN_SUB_COVERAGE = args.min_sub_coverage # = 0.1 # Threshold used to filter the features (gene or TE) mapped by a read = minimal subject coverage (nb of aligned bases / total nb of feature's bases)



    TE_by_chrom, counters = generate_counting(BAM_FILE, RM_TE_ANNOTATION_FILE, GENE_ANNOTATIONS_FILE)

    # Undefined Reads
    
    undef_df, classes = generate_undef_df(BAM_UNDEF_FILE, TE_by_chrom, GENE_ANNOTATIONS_FILE, TE_CLASSIFICATION_FILE)

    # Uniquely mapped reads

    counting_df = from_raw_counting_to_df(TE_by_chrom, TE_CLASSIFICATION_FILE)

    counting_df = counting_df.drop(columns="Reads")  


    save_counting_df(counting_df, OUTPUT_PREFIX + ".tsv")

    undef_df = undef_df.drop(columns="Reads")

    save_counting_df(undef_df, OUTPUT_PREFIX + ".undef.tsv")

    print_log(OUTPUT_PREFIX + ".log", counters, classes)

