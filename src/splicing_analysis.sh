/*Create a fasta file for all annotated TE insertions, and a fasta files for all annotated transcripts*/
gffread -w Dm_Goth_10-1.dmel6.23LiftOff.fa -g GCA_927717585.1.contig_named.fasta Dm_Goth_10-1.dmel6.23LiftOff.sorted.gff
less dm101-repeatcraft-A.rmerge.gtf | awk '{print $1"\t"$2"\texon\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9,$10,$11,$12,$13}' > dm101-repeatcraft-A.rmerge_exon.gtf
gffread -w dm101-repeatcraft-A.rmerge.fa -g ../GCA_927717585.1.contig_named.fasta dm101-repeatcraft-A.rmerge_exon.gtf

/*Concactenate TE and transcripts*/
cat ../Dm_Goth_10-1.dmel6.23LiftOff.fa dm101-repeatcraft-A.rmerge.fa  > Dm_Goth_10-1.dmel6.23LiftOff_dm101-repeatcraft-A.rmerge.fa

/*Align long reads to both TEs and transcripts*/
minimap2 -ax splice Dm_Goth_10-1.dmel6.23LiftOff_dm101-repeatcraft-A.rmerge.fa ../FC30.fastq.gz > FC30_mapped2_transcripts_and_TE.sam
minimap2 -ax splice Dm_Goth_10-1.dmel6.23LiftOff_dm101-repeatcraft-A.rmerge.fa ../FC29.fastq.gz > FC29_mapped2_transcripts_and_TE.sam

[M::main] Real time: 1376.980 sec; CPU: 4141.582 sec; Peak RSS: 3.413 GB
[M::main] Real time: 3559.807 sec; CPU: 10793.013 sec; Peak RSS: 3.327 GB
a total of 20Go

/*Keep only reads which map to TEs, with max AS*/
python3 ../filter_bam.py FC30_mapped2_transcripts_and_TE.sam FC30_mapped2_transcripts_and_TE_max_AS.bam FC30_mapped2_transcripts_and_TE.ambiguous_max_AS.bam
python3 ../filter_bam.py FC29_mapped2_transcripts_and_TE.sam FC29_mapped2_transcripts_and_TE_max_AS.bam FC29_mapped2_transcripts_and_TE.ambiguous_max_AS.bam

python3 SplicingAnalysis.py FC30_mapped2_transcripts_and_TE_max_AS.bam > FC30_mapped2_transcripts_and_TE_max_AS.introns
python3 SplicingAnalysis.py FC29_mapped2_transcripts_and_TE_max_AS.bam > FC29_mapped2_transcripts_and_TE_max_AS.introns

/*Retrieve the sequences of the potential splice sites*/
/data/home/vincent/FastaToTbl dm101-repeatcraft-A.rmerge.fa > dm101-repeatcraft-A.rmerge.tbl
awk '{if (NR==FNR){seq[$1]=$2} else {donor = substr(seq[$2],$9+1,2); acceptor = substr(seq[$2],$9+$10-1,2); print $0"\t"donor"\t"acceptor}}' dm101-repeatcraft-A.rmerge.tbl FC30_mapped2_transcripts_and_TE_max_AS.introns > FC30_mapped2_transcripts_and_TE_max_AS.introns.splice_sites
awk '{if (NR==FNR){seq[$1]=$2} else {donor = substr(seq[$2],$9+1,2); acceptor = substr(seq[$2],$9+$10-1,2); print $0"\t"donor"\t"acceptor}}' dm101-repeatcraft-A.rmerge.tbl FC29_mapped2_transcripts_and_TE_max_AS.introns > FC29_mapped2_transcripts_and_TE_max_AS.introns.splice_sites

/*Count number of reads, number of potential splice (N's in the CIGAR string), no-splice and GT-AG*/
less FC30_mapped2_transcripts_and_TE_max_AS.introns.splice_sites  | awk '{split($2,a,"$"); refL=a[4]-a[3]; nb[$2]+=1; nbspl[$2]+=($8!="-1"); nbnspl[$2]+=($8=="-1"); subcov[$2]+=$7/refL; nbGTAG[$2]+=($8!="-1" && $11=="GT" && $12=="AG"); nbCTAC[$2]+=($8!="-1" && $11=="CT" && $12=="AC");} END {for (i in nb){split(i,b,"_"); split(b[2],c,"$"); nbcanSpl[i]=nbGTAG[i]+nbCTAC[i]; if (nbspl[i]==0){percCan=-1} else {percCan=(nbcanSpl[i])/nbspl[i];} if (nbcanSpl[i]==0){percGTAG=-1} else {percGTAG=nbGTAG[i]/nbcanSpl[i]}  print i, nb[i],nbspl[i]/nb[i],nbspl[i],nbGTAG[i],nbCTAC[i],nbcanSpl[i],percCan,percGTAG,subcov[i]/nb[i], c[1]}}' > FC30_mapped2_transcripts_and_TE_max_AS.introns.splice_sites_perCopy.txt
less FC29_mapped2_transcripts_and_TE_max_AS.introns.splice_sites  | awk '{split($2,a,"$"); refL=a[4]-a[3]; nb[$2]+=1; nbspl[$2]+=($8!="-1"); nbnspl[$2]+=($8=="-1"); subcov[$2]+=$7/refL; nbGTAG[$2]+=($8!="-1" && $11=="GT" && $12=="AG"); nbCTAC[$2]+=($8!="-1" && $11=="CT" && $12=="AC");} END {for (i in nb){split(i,b,"_"); split(b[2],c,"$"); nbcanSpl[i]=nbGTAG[i]+nbCTAC[i]; if (nbspl[i]==0){percCan=-1} else {percCan=(nbcanSpl[i])/nbspl[i];} if (nbcanSpl[i]==0){percGTAG=-1} else {percGTAG=nbGTAG[i]/nbcanSpl[i]}  print i, nb[i],nbspl[i]/nb[i],nbspl[i],nbGTAG[i],nbCTAC[i],nbcanSpl[i],percCan,percGTAG,subcov[i]/nb[i], c[1]}}' > FC29_mapped2_transcripts_and_TE_max_AS.introns.splice_sites_perCopy.txt




/*I need to intersect with insertions that are expressed using our mapping to genome strategy, otherwise, I get cases where the reads map to a TE insertion, when it should map to the genome, at a location where there is another TE insertion, but not correctly annotated*/
less FC30.table.tsv | awk '($5+$6>1){print $4}' > FC30.expressedTEcopies.txt
sed 's/\$/\\$/g' FC30.expressedTEcopies.txt > FC30.expressedTEcopies_dollar.txt 

less FC29.table.tsv | awk '($5+$6>1){print $4}' > FC29.expressedTEcopies.txt
sed 's/\$/\\$/g' FC29.expressedTEcopies.txt > FC29.expressedTEcopies_dollar.txt 


grep -f FC30.expressedTEcopies_dollar.txt  FC30_mapped2_transcripts_and_TE_max_AS.introns.splice_sites_perCopy.txt > FC30_TEsplicing.txt
grep -f FC29.expressedTEcopies_dollar.txt  FC29_mapped2_transcripts_and_TE_max_AS.introns.splice_sites_perCopy.txt > FC29_TEsplicing.txt

R


pdf("SpliceFC30.pdf")
spliceFC30=read.table("FC30_TEsplicing.txt")
plot(spliceFC30$V3[spliceFC30$V2>4],spliceFC30$V8[spliceFC30$V2>4],cex=log10(spliceFC30$V2[spliceFC30$V2>4]),xlab="Proportion of Spliced Reads",ylab="TE Coverage",pch=21, col="purple")
points(spliceFC30$V3[spliceFC30$V2>4 & spliceFC30$V7>0.5],spliceFC30$V8[spliceFC30$V2>4 &  spliceFC30$V7>0.5],cex=log10(spliceFC30$V2[spliceFC30$V2>4 & spliceFC30$V7>0.5]),pch=21, bg="purple")
text(spliceFC30$V3[spliceFC30$V2>10],spliceFC30$V8[spliceFC30$V2>10],cex=0.65,pos=3,labels=spliceFC30$V9[spliceFC30$V2>10])
dev.off()

pdf("SpliceFC29.pdf")
spliceFC29=read.table("FC29_TEsplicing.txt")
plot(spliceFC29$V3[spliceFC29$V2>4],spliceFC29$V8[spliceFC29$V2>4],cex=log10(spliceFC29$V2[spliceFC29$V2>4]),xlab="Proportion of Spliced Reads",ylab="TE Coverage",pch=21, col="green")
points(spliceFC29$V3[spliceFC29$V2>4 & spliceFC29$V7>0.5],spliceFC29$V8[spliceFC29$V2>4 &  spliceFC29$V7>0.5],cex=log10(spliceFC29$V2[spliceFC29$V2>4 & spliceFC29$V7>0.5]),pch=21, bg="green")
text(spliceFC29$V3[spliceFC29$V2>30],spliceFC29$V8[spliceFC29$V2>30],cex=0.65,pos=3,labels=spliceFC29$V9[spliceFC29$V2>30])
dev.off()


pdf("SpliceFC29_focusSense.pdf")
spliceFC29m5=spliceFC29[spliceFC29$V2>9,]
plot(spliceFC29m5$V3[spliceFC29m5$V8<0.5],spliceFC29m5$V10[spliceFC29m5$V8<0.5],cex=log10(spliceFC29m5$V2[spliceFC29m5$V8<0.5]),xlab="Proportion of Spliced Reads",ylab="TE Coverage",pch=21,xlim=c(0,1.01),ylim=c(0,1.01),cex.lab=1.5)
points(spliceFC29m5$V3[spliceFC29m5$V8>0.5 & spliceFC29m5$V9<0.5],spliceFC29m5$V10[spliceFC29m5$V8>0.5 & spliceFC29m5$V9<0.5],cex=log10(spliceFC29m5$V2[spliceFC29m5$V8>0.5 & spliceFC29m5$V9<0.5]),pch=25)
points(spliceFC29m5$V3[spliceFC29m5$V8>0.5 & spliceFC29m5$V9>0.5],spliceFC29m5$V10[spliceFC29m5$V8>0.5 & spliceFC29m5$V9>0.5],cex=log10(spliceFC29m5$V2[spliceFC29m5$V8>0.5 & spliceFC29m5$V9>0.5]),pch=21,bg="green")
text(spliceFC29m5$V3[spliceFC29m5$V8>0.5 & spliceFC29m5$V9>0.5],spliceFC29m5$V10[spliceFC29m5$V8>0.5 & spliceFC29m5$V9>0.5],cex=0.9,pos=3,labels=spliceFC29m5$V11[spliceFC29m5$V8>0.5 & spliceFC29m5$V9>0.5])
dev.off()


pdf("SpliceFC30_focusSense.pdf")
spliceFC30m5=spliceFC30[spliceFC30$V2>4,]
plot(spliceFC30m5$V3[spliceFC30m5$V8<0.5],spliceFC30m5$V10[spliceFC30m5$V8<0.5],cex=log10(spliceFC30m5$V2[spliceFC30m5$V8<0.5]),xlab="Proportion of Spliced Reads",ylab="TE Coverage",pch=21,xlim=c(0,1.01),ylim=c(0,1.01),cex.lab=1.5)
points(spliceFC30m5$V3[spliceFC30m5$V8>0.5 & spliceFC30m5$V9<0.5],spliceFC30m5$V10[spliceFC30m5$V8>0.5 & spliceFC30m5$V9<0.5],cex=log10(spliceFC30m5$V2[spliceFC30m5$V8>0.5 & spliceFC30m5$V9<0.5]),pch=25)
points(spliceFC30m5$V3[spliceFC30m5$V8>0.5 & spliceFC30m5$V9>0.5],spliceFC30m5$V10[spliceFC30m5$V8>0.5 & spliceFC30m5$V9>0.5],cex=log10(spliceFC30m5$V2[spliceFC30m5$V8>0.5 & spliceFC30m5$V9>0.5]),pch=21,bg="purple")
text(spliceFC30m5$V3[spliceFC30m5$V8>0.5 & spliceFC30m5$V9>0.5],spliceFC30m5$V10[spliceFC30m5$V8>0.5 & spliceFC30m5$V9>0.5],cex=0.9,pos=3,labels=spliceFC30m5$V11[spliceFC30m5$V8>0.5 & spliceFC30m5$V9>0.5])
dev.off()


