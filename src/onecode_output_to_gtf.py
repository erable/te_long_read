"""Make a GTF from a one code output. Also, rename element to remove "-LTR" or "-I" suffixes.
ex:
2558    4.3     0.8     1.7     2L_RaGOO        1       356     353     +       IVK_DM  LINE/I  5010    5362    (40)    1       1       0.065
TO
2L_RaGOO        dmgoth101       exon    1       356     2558    +       .       gene_id "IVK_DM"; transcript_id "IVK_DM_2L_RaGOO_1_356";
"""

import sys  # Use shell argument

OC_FILE = sys.argv[1]  # Dm_Goth_10-1_onecode.v6.csv
NEW_GTF = sys.argv[2]  # dmgoth101.onecode.v6.gtf

with open(NEW_GTF, 'w') as output:
	with open(OC_FILE, 'r') as input:
		for line in input:
			sline = line.strip().split("\t")
			chrom, start, end, score, strand, gene_id, family = sline[4:11]  # get info from OneCode file
			# rename "gene_id" by deleting "LTR" and "I" suffixes
			if gene_id.endswith("_LTR"):
				gene_id = gene_id[:-4]
			elif gene_id.endswith("_I"):
				gene_id = gene_id[:-2]
			elif gene_id.endswith("-I_DM"):
				gene_id = gene_id[:-5] + "-DM"
			elif gene_id.endswith("-LTR_DM"):
				gene_id = gene_id[:-7] + "-DM"
			subclass, superfamily = family.split("/")
			source = "dmgoth101"  # has to be customizable to generalized the code
			feature = "exon"  # has to be customizable to generalized the code
			phase = "."
			transcript_id = "$".join([gene_id, chrom, start, end])
			attributes = 'gene_id "{}"; transcript_id "{}";'.format(gene_id, transcript_id)
			new_line = "\t".join([chrom, source, feature, start, end, score, strand, phase, attributes]) + "\n"
			output.write(new_line)



 # based on eric c. code, with addition