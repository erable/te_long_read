"""Rename contig name with more human-readable one.
ex : 
>ENA|CAKMKQ010000001|CAKMKQ010000001.1 Drosophila melanogaster strain dmgoth101 genome assembly, contig: 211000022278098_RaGOO
TO
211000022278098_RaGOO
"""

import sys  # Use shell argument

genome_path = sys.argv[1]

with open(genome_path, "r") as genome:
	for line in genome:
		line = line.strip()
		if line.startswith(">"):  # if the line is a sequence ID
			print(">" + line.split(" ")[-1])  # keep the last word
		else:
			print(line)
