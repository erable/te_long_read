#!/usr/bin/env python3

import pandas as pd
import argparse



def get_insertion_length(insertion_name):
    # parse the insertion name to compute the length
    start, end = insertion_name.split('$')[-2:]
    return int(end) - int(start) + 1


def create_summary_table(counting_table):   
    """
    Return a pd.DataFrame of information for each family (using insertion information)
    
    Args :
        counting_table (pd.DataFrame) : information about each insertion
        
    Return :
        pd.DataFrame : information by family
    """ 
    subclass_list = []
    superfamily_list = []
    family_list = counting_table["Family"].unique()
    min_TE_length_list = []
    max_TE_length_list = []
    counting_list = []
    nb_expressed_insertion_list = []
    nb_amb_expressed_insertion_list = []
    nb_insertion_list = []
    most_expressed_insertion_list = []
    for family in family_list :
        family_df = counting_table[counting_table["Family"] == family]
        subclass_list.append(family_df["Subclass"].values[0])
        superfamily_list.append(family_df["Superfamily"].values[0])
        min_TE_length = 9999999
        max_TE_length = 0
        i=0
        for insertion_name in family_df["Insertion"]:
            insertion_length = get_insertion_length(insertion_name)
            if insertion_length < min_TE_length :
                min_TE_length = insertion_length
            if insertion_length > max_TE_length :
                max_TE_length = insertion_length
        min_TE_length_list.append(min_TE_length)
        max_TE_length_list.append(max_TE_length)
        nb_insertion_list.append(len(family_df["Insertion"]))
        counting_list.append(family_df["Unique_Counts"].sum())
        nb_expressed_insertion = len(family_df[family_df["Unique_Counts"] > 0])
        family_df_without_counting = family_df[family_df["Unique_Counts"] <= 0]
        nb_amb_expressed_insertion = len(family_df_without_counting[family_df_without_counting["Multi-mapped_Counts"] > 0])
        nb_expressed_insertion_list.append(nb_expressed_insertion)  # minimum expressed insertion because undef cannot be quantified
        nb_amb_expressed_insertion_list.append(nb_amb_expressed_insertion)
        most_expressed_insertion = None if nb_expressed_insertion == 0 else family_df[family_df["Unique_Counts"] == family_df["Unique_Counts"].max()]["Insertion"].values[0]
        most_expressed_insertion_list.append(most_expressed_insertion)  # without ambiguous count ...

    summary_df = pd.DataFrame(list(zip(subclass_list, superfamily_list, family_list, nb_insertion_list, min_TE_length_list, max_TE_length_list, counting_list, nb_expressed_insertion_list, nb_amb_expressed_insertion_list, most_expressed_insertion_list)),
                               columns=["Subclass", "Superfamily", "Family", "nb_of_insertion", "Min_TE_length","Max_TE_length", "Counts (unique)", "nb_of_expressed_insertion", "number_of_potential_additional_expressed_insertions", "Most_expressed_insertion"])
    # Counts (unique) doesn't count yet the multi mapping
    return summary_df


def merge_female_and_male_df(male_counting, female_counting, male_undef, female_undef):
    female_df = create_summary_table(female_counting)
    male_df = create_summary_table(male_counting)
    hierarchy_cols = female_df[["Subclass", "Superfamily", "Family", "nb_of_insertion", "Min_TE_length","Max_TE_length"]]
    hierarchy_df = hierarchy_cols.copy()
    
    female_undef = female_undef[female_undef["Family"]!="undef"].drop(columns=["Insertion"])
    male_undef = male_undef[male_undef["Family"]!="undef"].drop(columns=["Insertion"])

    # Add unmapped read in famiy count    
    female_df["Counts (multi)"] = female_df.merge(female_undef, on=["Subclass","Superfamily","Family"], how="left").fillna(0)["Counting"].astype(int)
    male_df["Counts (multi)"] = male_df.merge(male_undef, on=["Subclass","Superfamily","Family"], how="left").fillna(0)["Counting"].astype(int)

    female_df["Counts (unique+multi)"] = female_df["Counts (unique)"] + female_df["Counts (multi)"]
    male_df["Counts (unique+multi)"] = male_df["Counts (unique)"] + male_df["Counts (multi)"]


    female_df = female_df.drop(columns=["Subclass", "Superfamily", "Min_TE_length","Max_TE_length", "nb_of_insertion"])
    male_df = male_df.drop(columns=["Subclass", "Superfamily", "Min_TE_length","Max_TE_length", "nb_of_insertion"])

    full_summary_df = hierarchy_df.merge(female_df, on='Family').merge(male_df, on='Family', suffixes=("_female","_male"))
    
    full_summary_df = full_summary_df[["Subclass","Superfamily","Family","nb_of_insertion","Min_TE_length","Max_TE_length","Counts (unique)_female","Counts (multi)_female","Counts (unique+multi)_female","nb_of_expressed_insertion_female","number_of_potential_additional_expressed_insertions_female","Most_expressed_insertion_female","Counts (unique)_male","Counts (multi)_male","Counts (unique+multi)_male","nb_of_expressed_insertion_male","number_of_potential_additional_expressed_insertions_male","Most_expressed_insertion_male"]]

    return full_summary_df


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('MALE_COUNTING_FILE', type=str, help='A tsv counting file for males generated by the "generate_counts" script.')
    parser.add_argument('FEMALE_COUNTING_FILE', type=str, help='A tsv counting file for females generated by the "generate_counts" script.')
    parser.add_argument('MALE_UNDEF_FILE', type=str, help='A tsv counting file for males generated by the "generate_counts" script.')
    parser.add_argument('FEMALE_UNDEF_FILE', type=str, help='A tsv counting file for females generated by the "generate_counts" script.')
    parser.add_argument('OUTPUT_COUNTING_TABLE', type=str, help='Ouput file for the merged counting table.')
    args = parser.parse_args()

    male_counting = pd.read_csv(args.MALE_COUNTING_FILE, sep = "\t")
    female_counting = pd.read_csv(args.FEMALE_COUNTING_FILE, sep = "\t")
    male_undef = pd.read_csv(args.MALE_UNDEF_FILE, sep = "\t")
    female_undef = pd.read_csv(args.FEMALE_UNDEF_FILE, sep = "\t")
    merged_full_summary_df = merge_female_and_male_df(male_counting, female_counting, male_undef, female_undef)
    merged_full_summary_df.to_csv(args.OUTPUT_COUNTING_TABLE, sep = '\t', index=False)
