"""Make a csv with the subclass, superfamily and family for each TE insertion present in the OC output."""

import sys  # Use shell argument

OC_FILE = sys.argv[1]  # Dm_Goth_10-1_onecode.v6.csv
CSV_OUTPUT = sys.argv[2]  # TE_classification.from_OC_output.v6.csv

with open(CSV_OUTPUT, 'w') as output:
	with open(OC_FILE, 'r') as input:
		for line in input:
			if len(line.split()) == 17 :
				sline = line.strip().split()
				chrom, start, end = sline[4:7]
				TE_id, TE_family = sline[9:11]
				# rename "gene_id" by deleting "LTR" and "I" suffixes
				if TE_id.endswith("_LTR"):
					TE_id = TE_id[:-4]
				elif TE_id.endswith("_I"):
					TE_id = TE_id[:-2]
				elif TE_id.endswith("-I_DM"):
					TE_id = TE_id[:-5] + "-DM"
				elif TE_id.endswith("-LTR_DM"):
					TE_id = TE_id[:-7] + "-DM"
				subclass, superfamily = TE_family.split("/")
				transcript_id = "$".join([TE_id, chrom, start, end])
				new_line = "\t".join([subclass, superfamily, TE_id, transcript_id]) + "\n"
				output.write( new_line)
