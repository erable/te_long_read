#!/usr/bin/env python3
import pysam
import argparse

# Argparse

parser = argparse.ArgumentParser()
parser.add_argument('sam_path', type=str, help='A sam alignment file. Need to be sorted by name.')
parser.add_argument('bam_max_as_output', type=str, help='Path of output (bam) with non ambiguous alignment with max as (for each read).')
parser.add_argument('bam_ambiguous', type=str, help='Path of output (bam) with all alignmet of ambiguous read when determine max as score..')
args = parser.parse_args()

sam_path = args.sam_path   # NEED TO BE SORT BY READ_NAME !
bam_max_as_output = args.bam_max_as_output
bam_ambiguous = args.bam_ambiguous

# FUNCTION

def write_ali(list_ali):
    """Write in the correct file the alignment. If there is one alignment with a best alignment
    score, write it on bam_as. Else, write all alignment with the best alignment score in bam_amb.
    
    Args:
    list_ali (list) : list of pysam.AlignedSegment
    bam_as (pysam.AlignmentFile) : the file with alignment with best alignment score
    bam_amb (pysam.AlignmentFile) : the file with ambiguous read """
    max = -9999
    best = []
    for ali in list_ali:
        score = ali.get_tag("AS")
        if score > max:
            max = score
            best = [ali]
        elif score == max:
            best.append(ali)
    if len(best) > 1:
        for ali in best:
            bam_amb.write(ali)
    else:
        bam_as.write(best[0])


# MAIN

with pysam.AlignmentFile(sam_path, "r") as sam:
    with pysam.AlignmentFile(bam_max_as_output, "wb", template=sam) as bam_as:
        with pysam.AlignmentFile(bam_ambiguous, "wb", template=sam) as bam_amb:
            for ali in sam:
                if not ali.is_mapped:
                    continue
                previous_read = ali.query_name
                list_ali = [ali]
                break  # cannot find how to just get the first read
            for ali in sam:
                if not ali.is_mapped:
                    continue
                current_read = ali.query_name
                if current_read == previous_read:
                    list_ali.append(ali)
                else:
                    write_ali(list_ali)
                    previous_read = current_read
                    list_ali = [ali]
            write_ali(list_ali)